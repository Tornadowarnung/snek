extends Node2D

signal game_over
signal score_changed(new_score: int)

@export var tile_size: int = 32
@export var tile_map_size: int = 8
@export var tick_time: float = 0.5
@export var start_body_count: int = 1

@export var player_scene: PackedScene = preload("res://game/snake.tscn")
@export var food_scene: PackedScene = preload("res://game/food.tscn")

@onready var eat_sound: RandomizedSoundPlayer = $EatSoundPlayer
@onready var game_over_sound: AudioStreamPlayer = $GameOverSoundPlayer
@onready var move_sound: RandomizedSoundPlayer = $MoveSoundPlayer

var score: int:
	get:
		return score
	set(value):
		score = value
		score_changed.emit(score)

var player: Node2D = null

var timer: Timer = Timer.new()


func _ready() -> void:
	randomize()
	player = player_scene.instantiate()
	add_child(player)
	player.spawn(tile_size, tile_map_size, start_body_count)
	player.food_eaten.connect(_on_food_eaten)
	player.died.connect(_show_game_over)
	
	add_child(timer)
	timer.timeout.connect(_on_tick)
	timer.start(tick_time)


func _on_food_eaten() -> void:
	if SettingsHolder.is_sound_enabled:
		eat_sound.play_next_sound()
	score += 1
	_spawn_food()


func _spawn_food() -> void:
	var player_parts: Array[Node] = get_tree().get_nodes_in_group("player")
	var player_tile_positions: Array[Vector2] = []
	for part: Node2D in player_parts:
		player_tile_positions.append(Vector2((part.position.x - 16) / tile_size, (part.position.y - 16) / tile_size))
	var free_positions: Array[Vector2] = []
	for i in range(tile_map_size):
		for j in range(tile_map_size):
			if player_tile_positions.has(Vector2(i, j)):
				continue
			else:
				free_positions.append(Vector2(i, j))
	var random_index: int = randi_range(0, free_positions.size() - 1)
	var tile_position: Vector2 = free_positions[random_index]
	var absolute_position: Vector2 = Vector2(tile_position.x * tile_size + 16, tile_position.y * tile_size + 16)
	var food: Node2D = food_scene.instantiate()
	add_child(food)
	food.position = absolute_position


func _on_tick() -> void:
	player.move()
	if SettingsHolder.is_music_enabled:
		move_sound.play_next_sound()


func _show_game_over() -> void:
	if SettingsHolder.is_sound_enabled:
		game_over_sound.play()
	timer.stop()
	game_over.emit()
