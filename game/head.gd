extends Area2D

@onready var sprite := $Sprite2D

var previous_position: Vector2 = Vector2(0, 0)

func look_to(direction: Vector2) -> void:
	if(direction == Vector2.UP):
		sprite.rotation_degrees = 0
	elif(direction == Vector2.RIGHT):
		sprite.rotation_degrees = 90
	elif(direction == Vector2.DOWN):
		sprite.rotation_degrees = 180
	elif(direction == Vector2.LEFT):
		sprite.rotation_degrees = 270


func move(direction: Vector2, tile_size: int) -> void:
	previous_position = position
	
	position += direction * tile_size
	look_to(direction)
