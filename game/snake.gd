extends Node2D

signal food_eaten
signal died

@onready var head: Node2D = $Head

var body_scene: PackedScene = preload("res://game/body.tscn")
var tile_size: int = 32
var tile_map_size: int = 8
var current_direction: Vector2 = Vector2.RIGHT

var bodies: Array = []

var next_steering: Vector2 = Vector2.ZERO

var direction_by_action: Dictionary = {
	"move right": Vector2.RIGHT,
	"move left": Vector2.LEFT,
	"move down": Vector2.DOWN,
	"move up": Vector2.UP
}


func _ready() -> void:
	head.position = head.position.snapped(Vector2.ONE * tile_size)
	head.position += Vector2.ONE * tile_size / 2
	head.add_to_group("player")


func _physics_process(_delta: float) -> void:
	if(head.has_overlapping_areas()):
		for area: Area2D in head.get_overlapping_areas():
			if !area.is_in_group("food"):
				continue
			area.queue_free()
			add_body()
			emit_signal("food_eaten")


func _unhandled_input(event: InputEvent) -> void:
	for dir: String in direction_by_action.keys():
		if event.is_action_pressed(dir):
			next_steering = direction_by_action[dir]


func spawn(tile_size_p: int, tile_map_size_p: int, start_body_count: int) -> void:
	tile_size = tile_size_p
	tile_map_size = tile_map_size_p
	@warning_ignore("integer_division")
	head.position = Vector2((start_body_count * tile_size) + (tile_size / 2), tile_size / 2)
	for i in range(1, start_body_count + 1):
		var body: Node2D = body_scene.instantiate()
		body.add_to_group("player")
		bodies.append(body)
		add_child(body)
		body.position = head.position
		body.position.x -= tile_size * i
		body.rotation_degrees = 90


func move() -> void:
	if next_steering == Vector2.ZERO || next_steering == -current_direction:
		next_steering = current_direction
	else:
		current_direction = next_steering
	
	head.move(next_steering, tile_size)
	
	for i: int in bodies.size():
		if i == 0:
			bodies[i].move(head.previous_position, head.position)
		else:
			bodies[i].move(bodies[i-1].previous_position, bodies[i-1].position)
	
	if(_is_colliding()):
		died.emit()


func add_body() -> void:
	var free_position: Vector2 = bodies[bodies.size() - 1].previous_position
	
	var body: Node2D = body_scene.instantiate()
	var previous_body: Node2D = bodies[bodies.size() - 1]
	bodies.append(body)
	add_child(body)
	body.spawn(free_position, previous_body.position)


func _is_colliding() -> bool:
	var current_tile_position: Vector2 = head.position / tile_size
	var is_colliding_with_wall: bool = current_tile_position.x > tile_map_size || current_tile_position.x < 0 || current_tile_position.y > tile_map_size || current_tile_position.y < 0
	
	var current_position: Vector2 = head.position
	var is_colliding_with_body: bool = false
	
	for body: Area2D in bodies:
		if body.position == current_position:
			is_colliding_with_body = true
	
	return is_colliding_with_wall || is_colliding_with_body
