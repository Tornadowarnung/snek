extends Area2D

@onready var sprite: Sprite2D = $Sprite2D
@onready var straight_texture: Resource = preload("res://assets/SnakeBody.png")
@onready var right_turn_texture: Resource = preload("res://assets/SnakeTurnRight.png")

var previous_position: Vector2 = Vector2(0, 0)


func _ready() -> void:
	add_to_group("player")


func move(move_to: Vector2, future_position: Vector2) -> void:
	previous_position = position
	position = move_to
	var current_direction: Vector2 = (position - previous_position).normalized()
	var future_direction: Vector2 = (future_position - move_to).normalized()
	if future_direction == current_direction:
		sprite.texture = straight_texture
		_handle_straight_rotation(current_direction)
	else:
		sprite.texture = right_turn_texture
		_handle_angled_rotation(current_direction, future_direction)


func spawn(move_to: Vector2, future_position: Vector2) -> void:
	position = move_to
	var direction: Vector2 = (future_position - position).normalized()
	_handle_straight_rotation(direction)


func _handle_straight_rotation(direction: Vector2) -> void:
	if direction == Vector2.UP || direction == Vector2.DOWN:
		rotation_degrees = 0
	elif direction == Vector2.LEFT || direction == Vector2.RIGHT:
		rotation_degrees = 90

func _handle_angled_rotation(current_direction: Vector2, future_direction: Vector2) -> void:
	if current_direction.angle_to(future_direction) < 0:
		sprite.flip_h = true
	else:
		sprite.flip_h = false
	
	if current_direction == Vector2.UP && future_direction == Vector2.RIGHT:
		rotation_degrees = 0
	elif current_direction == Vector2.RIGHT && future_direction == Vector2.DOWN:
		rotation_degrees = 90
	elif current_direction == Vector2.DOWN && future_direction == Vector2.LEFT:
		rotation_degrees = 180
	elif current_direction == Vector2.LEFT && future_direction == Vector2.UP:
		rotation_degrees = 270
	
	elif current_direction == Vector2.UP && future_direction == Vector2.LEFT:
		rotation_degrees = 0
	elif current_direction == Vector2.RIGHT && future_direction == Vector2.UP:
		rotation_degrees = 90
	elif current_direction == Vector2.DOWN && future_direction == Vector2.RIGHT:
		rotation_degrees = 180
	elif current_direction == Vector2.LEFT && future_direction == Vector2.DOWN:
		rotation_degrees = 270
