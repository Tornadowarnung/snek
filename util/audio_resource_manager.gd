extends Node


func load_sounds_from(directory: String) -> Array[AudioStreamWAV]:
	var result: Array[AudioStreamWAV] = []
	var dir: DirAccess = DirAccess.open(directory)
	for file_name: String in dir.get_files():
		if !file_name.contains(".wav") || file_name.contains(".import"):
			continue
		var sound_file: String = directory + file_name
		if FileAccess.file_exists(sound_file):
			result.append(load(sound_file))
	return result
