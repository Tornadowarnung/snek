extends Node


static var changable_actions: Array[String] = ["move right", "move left", "move up", "move down"]


func add_key_to_action(action: String, key: InputEvent) -> String:
	var assigned_action: Array[String] = _is_input_already_assigned(key)
	if assigned_action != []:
		var error: String = "Input <" + key.as_text() + "> is already assigned to " + str(assigned_action)
		print("InputMap: ", error)
		return error
	InputMap.action_add_event(action, key)
	return ""


func remove_key_from_action(action: String, key: InputEvent) -> String:
	var is_assigned: bool = InputMap.action_get_events(action).any(func (map_key: InputEvent) -> bool: return map_key.is_match(key))
	if (!is_assigned):
		return "Input <" + key.as_text() + "> is not assigned to " + action
	var is_last_input: bool = InputMap.action_get_events(action).size() <= 1
	if (is_last_input):
		return "Assign another input before removing the last one"
	InputMap.action_erase_event(action, key)
	return ""


func replace_key_for_action(action: String, current: InputEvent, updated: InputEvent) -> String:
	var assigned_action: Array[String] = _is_input_already_assigned_to(updated, changable_actions.filter(func(a: String) -> bool: return a != action))
	if assigned_action != []:
		var error: String = "Input <" + updated.as_text() + "> is already assigned to " + str(assigned_action)
		print("InputMap: ", error)
		return error
	InputMap.action_erase_event(action, current)
	InputMap.action_add_event(action, updated)
	return ""


func get_all_keyboard_inputs_for_action(action: String) -> Array[InputEventKey]:
	var result: Array[InputEventKey] = []
	result.append_array(InputMap.action_get_events(action).filter(func(event: InputEvent) -> bool : return event is InputEventKey))
	return result
	


func _is_input_already_assigned(key: InputEvent) -> Array[String]:
	return _is_input_already_assigned_to(key, changable_actions)


func _is_input_already_assigned_to(key: InputEvent, actions: Array[String]) -> Array[String]:
	return actions.filter(func(action: String) -> bool: return _is_input_in_action(key, action))


func _is_input_in_action(key: InputEvent, action: String) -> bool:
	return InputMap.action_get_events(action).any(func(original: InputEvent) -> bool: return _is_input_same(original, key))


func _is_input_same(original: InputEvent, param: InputEvent) -> bool:
	return original.is_match(param)
