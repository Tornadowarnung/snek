extends Node


func _ready() -> void:
	_scale_window_max()
	_move_to_screen_center()


func _scale_window_max() -> void:
	var current_window: Vector2i = DisplayServer.window_get_size()
	var screen: Vector2i = DisplayServer.screen_get_size()
	var scale_factor_width: float = float(screen.x) / float(current_window.x)
	var scale_factor_height: float = float(screen.y) / float(current_window.y)
	var scale_factor: int = floor(min(scale_factor_height, scale_factor_width))
	DisplayServer.window_set_size(current_window * scale_factor)


func _move_to_screen_center() -> void:
	var window: Vector2i = DisplayServer.window_get_size()
	var screen: Vector2i = DisplayServer.screen_get_size()
	var new_pos: Vector2i = screen / 2 - window / 2
	DisplayServer.window_set_position(new_pos)
