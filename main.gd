extends Node2D

@onready var ui: CanvasLayer = $UI
@onready var game: Node2D = $Game


func _ready() -> void:
	game.game_over.connect(_on_game_over)
	game.score_changed.connect(_on_score_changed)


func _on_game_over() -> void:
	ui.show_game_over()


func _on_score_changed(new_score: int) -> void:
	ui.update_score(new_score)
