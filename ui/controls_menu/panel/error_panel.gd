extends Panel

signal dismissed
signal closing

var error := ""


func _ready() -> void:
	$MarginContainer/Panel/Label.text = error


func _gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton or _is_escape(event):
		print("ErrorPanel _gui_input: ", event)
		dismissed.emit()
		queue_free()


func _unhandled_key_input(event: InputEvent) -> void:
	if event.is_pressed():
		print("ErrorPanel _unhandled_key_input: ", event)
		closing.emit()
		queue_free()


func _is_escape(event: InputEvent) -> bool:
	return event is InputEventKey and (event as InputEventKey).keycode == KEY_ESCAPE
