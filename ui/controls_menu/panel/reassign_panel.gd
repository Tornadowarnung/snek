extends Panel

signal key_assigned
signal dismissed


func _gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton or _is_escape(event):
		print("ReassignPanel _gui_input: ", event)
		queue_free()


func _unhandled_key_input(event: InputEvent) -> void:
	if event.is_pressed() and event is InputEventKey and !_is_escape(event):
		print("ReassignPanel _unhandled_key_input: ", event)
		key_assigned.emit(event)
		get_viewport().set_input_as_handled()
		queue_free()
	elif event.is_pressed() and _is_escape(event):
		print("ReassignPanel _unhandled_key_input: ", event)
		get_viewport().set_input_as_handled()
		dismissed.emit()
		queue_free()


func _is_escape(event: InputEvent) -> bool:
	return event is InputEventKey and (event as InputEventKey).keycode == KEY_ESCAPE
