extends Label

@export var action: String = ""

func _ready() -> void:
	var input_text: String = InputMap.action_get_events(action)[0].as_text()
	
	text = action + ": " + input_text
