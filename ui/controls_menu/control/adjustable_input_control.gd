class_name AdjustableInputControl extends MarginContainer

@onready var action_label: Label = $HBoxContainer/Label
@onready var button_container: Control = $HBoxContainer/ButtonContainer

signal input_map_changed
signal focused

var action: String

var _is_focused: bool = false:
	set(value):
		_is_focused = value
		queue_redraw()
		if(value):
			focused.emit()

var _should_grab_focus: bool = false


func _ready() -> void:
	var is_initialized := _check_correct_initialization()
	if !is_initialized:
		return
	action_label.text = action
	_repopulate_list()


func _process(_delta: float) -> void:
	if _should_grab_focus:
		_should_grab_focus = false
		await get_tree().create_timer(0).timeout
		_focus_buttons()


func _draw() -> void:
	if _is_focused:
		draw_rect(Rect2(Vector2(0, 0), size), Color.DEEP_PINK, false, 2.0)


func _check_correct_initialization() -> bool:
	if action == null or action == "":
		print("AdjustableInputControl: Warning: action has not bee initialized")
		return false
	return true


func _repopulate_list() -> void:
	for example in button_container.get_children():
		example.queue_free()
	
	for assigned_key: InputEventKey in InputMapper.get_all_keyboard_inputs_for_action(action):
		var button: ReassignButton = load("res://ui/controls_menu/button/reassign_button.tscn").instantiate()
		button.assigned_key = assigned_key
		button.assigned_action = action
		button.focus_entered.connect(_on_gain_focus)
		button.focus_exited.connect(_on_lose_focus)
		button.dismissed.connect(func () -> void: _should_grab_focus = true)
		button.action_changed.connect(func () -> void: 
			_repopulate_list()
			_should_grab_focus = true
		)
		button_container.add_child(button)
	var add_button: AssignButton = load("res://ui/controls_menu/button/assign_button.tscn").instantiate()
	add_button.assigned_action = action
	add_button.dismissed.connect(func () -> void: _should_grab_focus = true)
	add_button.action_changed.connect(func () -> void:
		_repopulate_list()
		_should_grab_focus = true	
	)
	add_button.focus_entered.connect(_on_gain_focus)
	add_button.focus_exited.connect(_on_lose_focus)
	button_container.add_child(add_button)


func _focus_buttons() -> void:
	print("refocusing buttons for ", action)
	var first_button: Button = button_container.get_children().front()
	print("focusing button: ", first_button.text)
	first_button.grab_focus()


func _on_gain_focus() -> void:
	_is_focused = true
	_should_grab_focus = false


func _on_lose_focus() -> void:
	_is_focused = false
