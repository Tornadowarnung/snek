@tool
extends Control

@export var label_text := "move up"
@export var keys := ["up", "w", "+"]

@onready var button_container := $AdjustableInputControl/HBoxContainer/ButtonContainer
@onready var label := $AdjustableInputControl/HBoxContainer/Label
@onready var input_preview := load("res://ui/controls_menu/button/input_button_preview.tscn") as PackedScene


func _ready() -> void:
	if Engine.is_editor_hint():
		label.text = label_text
		for key: String	 in keys:
			var button := input_preview.instantiate()
			button.key = key
			button_container.add_child(button)
	else:
		queue_free()
