extends Control


@onready
var input_container: Control = $ScrollContainer/InputContainer

@onready
var adjustable_input_control_scene: PackedScene = load("res://ui/controls_menu/control/adjustable_input_control.tscn")


func _ready() -> void:
	_remove_example_inputs()
	_initialize_changable_inputs()

	get_viewport().gui_focus_changed.connect(func (control: Control) -> void: print("focus changed to ", control))


func _remove_example_inputs() -> void:
	for example in input_container.get_children():
		example.queue_free()


func _initialize_changable_inputs() -> void:
	for action in InputMapper.changable_actions:
		var control := adjustable_input_control_scene.instantiate() as AdjustableInputControl
		control.action = action
		input_container.add_child(control)
		
		control.focused.connect(func () -> void: ($ScrollContainer as ScrollContainer).ensure_control_visible(control))
		
