class_name ReassignButton extends Button

signal action_changed
signal dismissed

var assigned_key: InputEvent
var assigned_action: String


var _is_focused: bool = false


func _ready() -> void:
	_check_correct_initialization()

	text = OS.get_keycode_string(assigned_key.keycode)
	pressed.connect(_on_pressed)
	focus_entered.connect(func () -> void: _is_focused = true)
	focus_exited.connect(func () -> void: _is_focused = false)
	$TextureButton.pressed.connect(_on_delete_button_pressed)


func _unhandled_input(event: InputEvent) -> void:
	if(event.is_action_pressed("ui_delete") and _is_focused):
		_on_delete_button_pressed()


func _check_correct_initialization() -> void:
	if(assigned_key == null):
		print("ReassignButton: Error: No action assigned")
		breakpoint
	if(assigned_action == null or assigned_action == ""):
		print("ReassignButton: Error: No action assigned")
		breakpoint


func _on_pressed() -> void:
	print("ReassignButton: _on_pressed")
	var updated_key: InputEvent = await _show_reassign_panel()
	var error: String = InputMapper.replace_key_for_action(assigned_action, assigned_key, updated_key)
	if error != "":
		await _show_error_panel(error)
	action_changed.emit()


func _on_delete_button_pressed() -> void:
	var error: String = InputMapper.remove_key_from_action(assigned_action, assigned_key)
	if error != "":
		await _show_error_panel(error)
	action_changed.emit()


func _show_reassign_panel() -> Signal:
	var popup: Panel = load("res://ui/controls_menu/panel/reassign_panel.tscn").instantiate()
	var available_size: Vector2 = get_viewport_rect().size
	popup.size = Vector2i(available_size)
	get_tree().get_root().add_child(popup)
	popup.show()
	popup.focus_mode = Control.FOCUS_ALL
	popup.grab_focus()
	popup.dismissed.connect(func () -> void: dismissed.emit())
	return popup.key_assigned


func _show_error_panel(error: String) -> Signal:
	var popup: Panel = load("res://ui/controls_menu/panel/error_panel.tscn").instantiate()
	var available_size: Vector2 = get_viewport_rect().size
	popup.size = Vector2i(available_size)
	popup.error = error
	get_tree().get_root().add_child(popup)
	popup.focus_mode = Control.FOCUS_ALL
	popup.grab_focus()
	popup.show()
	popup.dismissed.connect(func () -> void: dismissed.emit())
	return popup.closing
