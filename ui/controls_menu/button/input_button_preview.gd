@tool
extends Button

@export var key := "Up"


func _ready() -> void:
	if Engine.is_editor_hint():
		text = key
	else:
		queue_free()
