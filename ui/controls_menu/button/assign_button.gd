class_name AssignButton extends Button

signal action_changed
signal dismissed


var assigned_action: String


func _ready() -> void:
	_check_correct_initialization()

	text = "+"
	pressed.connect(_on_pressed)


func _check_correct_initialization() -> void:
	if(assigned_action == null or assigned_action == ""):
		print("AssignButton: Error: No action assigned")
		breakpoint


func _on_pressed() -> void:
	var updated_key: InputEvent = await _show_assign_panel()
	var error: String = InputMapper.add_key_to_action(assigned_action, updated_key)
	if error != "":
		await _show_error_panel(error)
	action_changed.emit()


func _show_assign_panel() -> Signal:
	var popup: Panel = load("res://ui/controls_menu/panel/reassign_panel.tscn").instantiate()
	var available_size: Vector2 = get_viewport_rect().size
	popup.size = Vector2i(available_size)
	get_tree().get_root().add_child(popup)
	popup.show()
	popup.focus_mode = Control.FOCUS_ALL
	popup.grab_focus()
	popup.dismissed.connect(func() -> void: dismissed.emit())
	return popup.key_assigned


func _show_error_panel(error: String) -> Signal:
	var popup: Panel = load("res://ui/controls_menu/panel/error_panel.tscn").instantiate()
	var available_size: Vector2 = get_viewport_rect().size
	popup.size = Vector2i(available_size)
	popup.error = error
	get_tree().get_root().add_child(popup)
	popup.focus_mode = Control.FOCUS_ALL
	popup.grab_focus()
	popup.show()
	popup.dismissed.connect(func () -> void: dismissed.emit())
	return popup.closing
