extends Button

@onready
var game_scene: PackedScene = load("res://main.tscn")


func _ready() -> void:
	pressed.connect(_on_click)
	grab_focus()


func _on_click() -> void:
	get_tree().change_scene_to_packed(game_scene)
