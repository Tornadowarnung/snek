extends TextureButton


func _ready() -> void:
	pressed.connect(_on_pressed)
	
	if !SettingsHolder.is_music_enabled:
		button_pressed = true


func _on_pressed() -> void:
	SettingsHolder.is_music_enabled = !SettingsHolder.is_music_enabled
