extends Button

@onready
var controls_menu: PackedScene = load("res://ui/controls_menu/controls_menu.tscn")


func _ready() -> void:
	pressed.connect(_on_pressed)


func _on_pressed() ->void:
	get_tree().change_scene_to_packed(controls_menu)
