extends Button

@export
var navigate_to: PackedScene


func _ready() -> void:
	pressed.connect(_on_pressed)
	grab_focus()


func _on_pressed() -> void:
	get_tree().change_scene_to_packed(navigate_to)
