extends CanvasLayer

@onready var center: Control = $Center
@onready var score_label: Label = $Top/HBoxContainer/Score
@onready var restart_button: Button = $Center/CenterContainer/VBoxContainer/ButtonContainer/VBoxContainer/RestartButton


func _ready() -> void:
	reset()


func show_game_over() -> void:
	center.visible = true
	restart_button.grab_focus()


func update_score(new_score: int) -> void:
	score_label.text = str(new_score)


func reset() -> void:
	center.visible = false
