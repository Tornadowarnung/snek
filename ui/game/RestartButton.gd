extends Button

@onready
var game_scene: PackedScene = load("res://main.tscn")


func _ready() -> void:
	pressed.connect(_on_pressed)


func _on_pressed() -> void:
	get_tree().change_scene_to_packed(game_scene)
