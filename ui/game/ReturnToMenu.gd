extends Button

@onready
var menu_scene: PackedScene = load("res://ui/main_menu/main_menu.tscn")


func _ready() -> void:
	pressed.connect(_on_pressed)


func _on_pressed() -> void:
	get_tree().change_scene_to_packed(menu_scene)
