extends AudioStreamPlayer
class_name RandomizedSoundPlayer


@export var sound_path: String = "res://sound/eat/"
@export var prevent_direct_repeat: bool = false

var sounds: Array[AudioStreamWAV] = []

var _previous_sound_index: int = -1


func _ready() -> void:
	randomize()
	sounds = AudioResourceManager.load_sounds_from(sound_path)


func play_next_sound() -> void:
	var sound_index: int = randi() % sounds.size()
	if sounds.size() > 1:
		while sound_index == _previous_sound_index:
			sound_index = randi() % sounds.size()
	var next_sound: AudioStreamWAV = sounds[sound_index]
	stream = next_sound
	play()

	_previous_sound_index = sound_index
